# Wayve Code Test

Clone this project with `git clone https://ketanpatel@bitbucket.org/ketanpatel/wayve_code_test.git`

`bundle install`

`rspec` to run tests.

Tests are in the `spec` folder

Module with the methods is in `lib/wayve_code_test.rb`

Test triangles are in `spec/data`
