require 'spec_helper'
require './lib/wayve_code_test'

describe "Max Triangle Sum" do
  include WayneCodeTest::MaxTriangleSum

  before :all do
    # [[4][7,4][2,4,6][8,5,9,3]]
    @valid_triangle = load_triangle './spec/data/example_triangle.txt'
  end

  it "should return 24" do
    expect(max_triangle_sum @valid_triangle).to eq 24
  end

  it "should only accept triangles that increase by one in size for each row" do
    invalid_triangle = [[4],[2,4,6],[8,5,9,3]]

    expect(validate_triangle @valid_triangle).to be true
    expect{validate_triangle invalid_triangle}.to raise_exception 'Invalid row sizes'
  end

  it "should only accept triangles that are full of Fixnums" do
    invalid_triangle = [[4],[7,4],[2,'rt',6],[8,5,';',3]]

    expect(validate_triangle @valid_triangle).to be true
    expect{validate_triangle invalid_triangle}.to raise_exception 'Invalid data in triangle'
  end

  it "should be able to run a 500 row triangle in under a minute" do
    start_time = Time.now
    total = max_triangle_sum load_triangle './spec/data/big_triangle.txt'
    end_time = Time.now

    time_taken = end_time.to_f - start_time.to_f

    expect(time_taken).to be < 60
  end

end