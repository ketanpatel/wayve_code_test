require 'spec_helper'
require './lib/wayve_code_test'

RSpec::Matchers.define :be_boolean do
  match do |actual|
    [true,false].include? actual
  end
end

describe "Palindrome" do
  include WayneCodeTest::Palindrome

  it "should return a boolean" do
    expect(palindrome? 'test').to be_boolean
  end

  it "should return true if word is a palindrome" do
    expect(palindrome? 'bob').to be true
  end

  it "should return false if word is not a palindrome" do
    expect(palindrome? 'tent').to be false
  end

  it "should ignore case by default" do
    expect(palindrome? 'Bob').to be true
  end

  it "should not ignore case if ignore_case is set to false" do
    expect(palindrome?('Bob', ignore_case: false)).to be false
  end

end