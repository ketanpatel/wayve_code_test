require 'spec_helper'
require './lib/wayve_code_test'

describe "Longest String" do
  include WayneCodeTest::LongestString

  let(:words) { { word1: 'this', word2: 'is',
                  word3: 'a',    word4: 'collection',
                  word5: 'of',   word6: 'words' } }

  before do

  end

  it "should return the longest string in a given hash" do
    expect(longest_string words).to eq "collection"
  end

end