require 'pry'

module WayneCodeTest

  module Palindrome

    def palindrome? string, ignore_case: true
      # Create reverse of string without using reverse
      reverse_string = ""
      (string.length - 1).downto(0) do |char|
        reverse_string += string[char]
      end

      if ignore_case
        string.downcase == reverse_string.downcase
      else
        string == reverse_string
      end
    end

  end

  module LongestString

    def longest_string hash
      hash.values.reduce { |longest, word| word.length > longest.length ? word : longest }
    end

  end

  module MaxTriangleSum

    def max_triangle_sum triangle
      validate_triangle triangle

      previous_index = 0
      sum = 0
      triangle.each do |row|
        if row.count == 1
          sum += row[0]
          next
        end

        if row[previous_index] > row[previous_index + 1]
          sum += row[previous_index]
        else
          sum += row[previous_index + 1]
          previous_index += 1
        end
      end

      sum
    end

    # Helper methods

    def load_triangle filename
      triangle = []
      File.open(filename).each do |line|
        row = []
        line.split(',').map { |number|row.push number.to_i }
        triangle.push row
      end
      triangle
    end

    def validate_triangle triangle
      previous_count = 1

      triangle.each_with_index do |row, i|
        row.each do |num|
          raise 'Invalid data in triangle' unless num.class == Fixnum
        end

        if i > 0
          unless row.count == previous_count + 1
            raise 'Invalid row sizes'
          end
          previous_count = row.count
        end

      end

      true
    end

  end
end


